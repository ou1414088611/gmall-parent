package com.atguigu.gmall.gateway;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * 网关（使用响应式编程（Reactor）== Webflux）
 *  1、注册中心
 *  2、服务发现/注册
 * @SpringBootApplication
 * @EnableDiscoveryClient
 * @EnableCircuitBreaker
 *
 * 默认报错：
 * 1、排除数据库的自动配置
 * 2、nacos客户端版本适配
 */


@SpringCloudApplication
public class GatewayMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayMainApplication.class,args);
    }
}
