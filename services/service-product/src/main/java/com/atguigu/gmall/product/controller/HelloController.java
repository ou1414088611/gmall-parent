package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


/**
 * @Controller
 * @ResponseBody
 */
@RestController
public class HelloController {


    @GetMapping("/hello")
    public Result hello(){
        Map<String,String> map = new HashMap<>();
        map.put("test","hello");
        return Result.ok(map);
    }
}
