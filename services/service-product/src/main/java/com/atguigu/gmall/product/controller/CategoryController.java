package com.atguigu.gmall.product.controller;


import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.BaseCategory1;
import com.atguigu.gmall.model.product.BaseCategory2;
import com.atguigu.gmall.model.product.BaseCategory3;
import com.atguigu.gmall.product.service.BaseCategory1Service;
import com.atguigu.gmall.product.service.BaseCategory2Service;
import com.atguigu.gmall.product.service.BaseCategory3Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 分类控制器
 */
@RequestMapping("/admin/product")
@RestController
public class CategoryController {

    @Autowired
    BaseCategory1Service category1Service;

    @Autowired
    BaseCategory2Service category2Service;

    @Autowired
    BaseCategory3Service category3Service;


    /**
     * 获取所有一级分类
     * @return
     */
    @GetMapping("/getCategory1")
    public Result<List<BaseCategory1>> getCategory1s(){
        System.out.println("执行了没..................");
        List<BaseCategory1> list = category1Service.list();
        return Result.ok(list);
    }


    /**
     * 获取二级分类
     */
    @GetMapping("/getCategory2/{category1Id}")
    public Result getCategory2s(@PathVariable("category1Id") Long category1Id){

        List<BaseCategory2> category2s = category2Service.getCategory2By1Id(category1Id);

        return Result.ok(category2s);
    }

    /**
     * 获取三级分类
     */
    @GetMapping("/getCategory3/{category2Id}")
    public Result getCategory3s(@PathVariable("category2Id")Long category2Id){

        List<BaseCategory3> category3s = category3Service.getCategory3By2Id(category2Id);
        return Result.ok(category3s);
    }


}
